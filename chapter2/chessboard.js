/*
„Write a program that creates a string that represents an 8×8 grid, using newline characters
to separate lines. At each position of the grid there is either a space or a "#" character.
The characters should form a chessboard.

Passing this string to console.log should show something like this:

 # # # #
# # # #
 # # # #
# # # #
 # # # #
# # # #
 # # # #
# # # #

When you have a program that generates this pattern, define a binding size = 8 and change
the program so that it works for any size, outputting a grid of the given width and height.“

Source: Marijn Haverbeke. „Eloquent JavaScript.“ Apple Books.
*/
let size = 8, currentChar, line, first, second;
for (let rows=1; rows <= size; rows++) {
    line = "", first = "#", second = " ";
    if (rows % 2 === 0) {
        first = " ";
        second = "#";
    }
    for (let cols=1; cols <= size; cols++) {
        currentChar = first;
        if (cols % 2 === 0) currentChar = second;
        line = line + currentChar;
    }
    console.log(line);
}
